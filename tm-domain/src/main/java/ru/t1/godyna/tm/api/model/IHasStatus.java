package ru.t1.godyna.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
