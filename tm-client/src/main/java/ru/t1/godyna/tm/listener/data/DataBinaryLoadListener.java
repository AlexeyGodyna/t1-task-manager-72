package ru.t1.godyna.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.domain.DataBinaryLoadRequest;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.event.ConsoleEvent;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    private final static String NAME = "data-load-bin";

    @NotNull
    private final static String DESCRIPTION = "Load data from binary file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinaryLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BINARY LOAD]");
        domainEndpoint.loadDataBinary(new DataBinaryLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
