package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.SessionDTO;
import ru.t1.godyna.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable SessionDTO session);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
