package ru.godyna.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.godyna.tm.marker.IntegrationCategory;
import ru.godyna.tm.model.Result;
import ru.godyna.tm.model.Task;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @NotNull
    private final Task task1 = new Task("Test Task1");

    @NotNull
    private final Task task2 = new Task("Test Task2");

    @NotNull
    private final Task task3 = new Task("Test Task3");

    @NotNull
    private final Task task4 = new Task("Test Task4");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        System.out.println(response);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<Task> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Task.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task3, header));
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Test
    public void saveTest() {
        @NotNull final String expected = task4.getName();
        @NotNull final String url = BASE_URL + "save/";
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(task4, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task task = response.getBody();
        Assert.assertNotNull(task);
        @NotNull final String actual = task.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void findByIdTest() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = BASE_URL + "findById/" + id;
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task project = response.getBody();
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(id, actual);
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = BASE_URL + "deleteById/" + id;
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
        @NotNull final String urlFind = BASE_URL + "findById/" + id;
        Assert.assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

}
